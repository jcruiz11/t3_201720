package model.vo;

import java.util.Iterator;

import model.data_structures.Queue;

public class BusUpdateVO 
{
	private Queue<Bus> update;
	public class Bus
	{
		private String VehicleNo;
		private String TripId;
		private String RouteNo;
		private String Direction;
		private String Destination;
		private String Pattern;
		private Double Latitude;
		private Double Longitude;
		private String RecordedTime;
		private String Href;
		
		public Bus(String a,String b, String c,String d, String e, String f, Double g, Double h, String i, String j )
		{
			VehicleNo = a;
			TripId = b;;
			RouteNo = c;
			Direction = d;
			Destination = e;
			Pattern = f;
			Latitude = g;
			Longitude = h;
			RecordedTime = i;
			Href= j;
		}
	}
	public void set(String a,String b, String c,String d, String e, String f, Double g, Double h, String i, String j)
	{
		update.enqueue(new Bus(a, b, c, d, e, f, g, h, i, j));
	}
	
	public Double[] getLatLon(int tripID)
	{
		Double[] latlon = {0.0,0.0};
		Iterator<Bus> it = update.iterator();
		while(it.hasNext())
		{
			Bus bus = it.next();
			if(Integer.parseInt(bus.TripId) == tripID)
			{
				latlon[0] = bus.Latitude;
				latlon[1] = bus.Longitude;
			}
		}
		return latlon;
	}
	
}
