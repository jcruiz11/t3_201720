package model.vo;

public class StopVO 
{

	private String id;
	private String code;
	private String name;
	private String desc;
	private Double lat;
	private Double lon;
	private String zoneid;
	private String url;
	private String locationtype;
	private String parentstation;
	
	public void set(String a, String b, String c, String d, Double e, Double f, String g, String h, String i, String j)
	{
		id = a;
		code = b;
		name = c;
		desc = d;
		lat = e;
		lon = f;
		zoneid = g;
		url = h;
		locationtype = i;
		parentstation = j;
	}
	
	/**
	 * @return id - stop's id
	 */
	public int id() {
		// TODO Auto-generated method stub
		return Integer.parseInt(id);
	}

	/**
	 * @return name - stop name
	 */
	public String getName() {
		// TODO Auto-generated method stub
		return name;
	}
	
	public Double getLat()
	{
		return lat;
	}
	
	public Double getLon()
	{
		return lon;
	}
	
	public String getId()
	{
		return id;
	}
	
	public String getZoneId()
	{
		return zoneid;
	}

}

