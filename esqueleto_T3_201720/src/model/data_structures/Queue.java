package model.data_structures;

import java.util.Iterator;



public class Queue<E> implements IQueue<E>
{
  public Node<E> first,last;
	
  public Queue()
  {
	  first = null;
	  last = first;
  }
  
  
  private class Node<E>
   {
	   E object;
	   Node next;
   }
	public Iterator iterator() {
		// TODO Auto-generated method stub
		return new ListIterator();
	}
	private class ListIterator implements Iterator<E>
	{
       public Node<E> current = first;
		public boolean hasNext() 
		{
			// TODO Auto-generated method stub
			return current !=null;
		}

		public E next() 
		{
			Node<E> temp = current;
			current = current.next;
			return temp.object;
		}
		
	}

	public E dequeue()
	{
		if (isEmpty())
		{
			return null;
		}
		
		E item =  first.object;
		first = first.next;
		return item;
		
	}

	public void enqueue(E item) {
		Node oldLast = last;
		last = new Node();
		last.object=item;
		last.next=null;
		if(isEmpty())
		{
			first = last;
		}
		else
		{
			oldLast.next=last;
		}
		
	}

	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return first ==null;
	}

	public int size() 
	{
		int size = 0;
		Iterator<E> iter = iterator();
		while (iter.hasNext())
		{
			size++;
			iter.next();
		}
		return size;
	}
	public E darItem(Node<E> nodo)
	{
		return nodo.object;
	}

}
