package model.data_structures;

import java.util.Iterator;

public class Stack<E> implements IStack<E>
{
public Node<E> first ;

public  Stack ( )
{
	first = null;
}

private class Node<E>
	{
		E object;
		Node next;
		
	}
	
	public Iterator<E> iterator() 
	{
		// TODO Auto-generated method stub
		return new ListIterator();
	}
    private class ListIterator implements Iterator<E>
    {
          public Node<E> current = first;
		public boolean hasNext()
		{
			return current!=null;
		}

		public E next() {
			// TODO Auto-generated method stub
			Node<E> temp = current;
			current = current.next;
			return temp.object;
		}
    	
    }
	

	public E pop() {
		// TODO Auto-generated method stub
		if (isEmpty())
		{
			return null;
		}
		else
		{
		
		E item = first.object;
		first = first.next;
		return item;
		}
		}

	public void push(E item) 
	{
		// TODO Auto-generated method stub
		if (isEmpty())
		{
			first = new Node();
			first.object = item;
			first.next = null;
		}
		else
		{
		
		Node<E> oldFirst =first;
		first = new Node();
		first.object = item;
		first.next = oldFirst;
	
		}
		}

	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return first == null;
	}

	public int size() 
	{
		// TODO Auto-generated method stub
		int size = 0;
		Iterator<E> iter = iterator();
		while (iter.hasNext())
		{
			size++;
			iter.next();
		}
		return size;
	}


}
