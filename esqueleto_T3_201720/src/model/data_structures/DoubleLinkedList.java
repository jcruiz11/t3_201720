package model.data_structures;

import java.util.HashSet;
import java.util.Iterator;

import model.data_structures.Node;

public class DoubleLinkedList< T >  implements IList <T>

{
	public Node<T> first;
	public Node<T> last;
	
	
	public  DoubleLinkedList()
	{
		first = null;
		last = first;
		
	}
	public int getSize() 
	{
		int size = 0;
		Iterator<T> iter = iterator();
		while (iter.hasNext())
		{
			size++;
			iter.next();
		}
		return size;
		
	}
	public void addAtEnd(T aAgregar) 
	{
		if (isEmpty())
		{
			Node<T> last  = new Node<T>(aAgregar,null,null);
			first  = last;
		}
		else
		{
		
		Node<T> oldLast = last;
		Node<T> nuevo = new Node<T>(aAgregar,oldLast,null);
		oldLast.cambiarSiguiente(nuevo);
		last = nuevo;
		}
	}

	


	public Node<T> getElement(T element) 
	{
		Node<T> actual = first;
		Iterator<T> iter = iterator();
		boolean encontrado = false;
		while (!encontrado&& iter.hasNext())
		{
			T current = iter.next();
			if (current.equals(element))
			{
				encontrado=true;
			}
			else
			{
			actual = actual.darSiguiente();
			
			}
			}
		if (!encontrado)
		{
			return null;
		}
		else return actual;
	}

	public T getCurrentElement(Node<T> nodo) 
	{
		return nodo.objeto;
	              
	}

	public void delete() 
	{
		
	if (!isEmpty())
	{
		first = first.darSiguiente();
		first.cambiarAnterior(null);
	}	
		// TODO Auto-generated method stub
	}



	





	public void add(T aAgregar) 
	{
		Node<T> temp = new Node(aAgregar,first,null);
		if (!isEmpty())
		{
			first.cambiarAnterior(temp);;
		}
		first = temp;
	   if (last == null)
	   {
		   last = temp;
	   }
	}
	public void AddAtK(T anterior, T objeto)
	{
		if (!isEmpty())
		{
		Node<T> actual = first;
		Iterator<T> iter = iterator();
		boolean encontrado = false;
		while (!encontrado && iter.hasNext())
		{
			 T current = iter.next();
			 
			if (current.equals(anterior))
			{
				 Node<T> nuevo = new Node<T>(objeto,actual,actual.darSiguiente());
				  actual.cambiarSiguiente(nuevo);
				  nuevo.darSiguiente().cambiarAnterior(nuevo);
				  encontrado = true;
			}
			else 
			{
			
			
			actual = actual.darSiguiente();
			
			}
			}
		}

	}
	public void deleteAtK(T aEliminar) 
	{
		if (!isEmpty())
		{
		
		// TODO Auto-generated method stub
		Node<T> actual = first;
		Iterator<T> iter = iterator();
		boolean encontrado = false;
		while (!encontrado&& iter.hasNext())
		{
			T current = iter.next();
			if (current.equals(aEliminar))
			{
				 actual.darAnterior().cambiarSiguiente(actual.darSiguiente());
			      actual.darSiguiente().cambiarAnterior(actual.darAnterior());
			    encontrado = true;
			}
			else
			{
			actual = actual.darSiguiente();
			}
		}
	
	
		}
	}
	

	public T next(Node<T> actual) 
	{
		// TODO Auto-generated method stub
		return actual.darSiguiente().darObjeto();
	}
	public T previous(Node<T> actual) {
		// TODO Auto-generated method stub
		return actual.darAnterior().darObjeto();
	}





	public Iterator<T> iterator() 
	{
		return new ListIterator();
	}
	private class ListIterator implements Iterator<T>
	{
       public Node<T> current = first;
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return current != null;
		}

		public T next() {
		     Node<T> temp = current;
		     current = current.darSiguiente();
			return temp.objeto;
		}
		
	}
	
public boolean isEmpty()
{
	return first ==null;
}

	






	





	
}
