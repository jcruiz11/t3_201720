package model.data_structures;



public class Node <T >
{
  public T objeto;
   public Node<T> anterior;
   public Node <T>siguiente;
   
   public Node(T pObjeto, Node<T> pAnterior,Node<T> pSiguiente)
	{
		this.objeto = pObjeto;
		this.anterior = pAnterior;
		this.siguiente = pSiguiente;
	}
   public void cambiarAnterior(Node<T> nuevo)
   {
	   anterior = nuevo;
   }
   public void cambiarSiguiente(Node<T> nuevo)
   {
	   siguiente = nuevo;
   }
    public T darObjeto()
   {
	   return  objeto;
   }
   public Node<T> darAnterior()
   {
	   return anterior;
   }
   public Node<T> darSiguiente()
   {
	   return siguiente;
   }
}
