package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.Iterator;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IStack;
import model.data_structures.Queue;
import model.data_structures.RingList;
import model.data_structures.Stack;
import model.vo.BusUpdateVO;
import model.vo.BusUpdateVO.Bus;
import model.vo.StopVO;
import api.ISTSManager;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import org.json.simple.parser.JSONParser;



public class STSManager implements ISTSManager{

	public BusUpdateVO busupdate;
	public RingList<zoneid> stops;
	
	class zoneid
	{
		public String zone;
		public DoubleLinkedList<StopVO> stopszone;
		public zoneid()
		{
			stopszone = new DoubleLinkedList<StopVO>(); 
		}
		public void addStop(StopVO stopToAdd)
		{
			Iterator<StopVO> itstop = stopszone.iterator();
			boolean a = false;
			while(itstop.hasNext() && !a)
			{
				StopVO stop = itstop.next();
				if(stopToAdd.getId().compareTo(stop.getId()) >= 0)
				{
					stopszone.AddAtK(stop, stopToAdd);
					a = true;
				}
			}
			if(!a) {stopszone.add(stopToAdd);}
		}
		public DoubleLinkedList<StopVO> getList()
		{
			return stopszone;
		}
	}
	
	public void readBusUpdate(File rtFile) {
		// TODO Auto-generated method stub
		JSONParser parser = new JSONParser();
		busupdate = new BusUpdateVO();
		try
		{
			JSONArray jsa = (JSONArray) parser.parse(new FileReader(rtFile));
			for(Object o : jsa)
			{
				JSONObject bus = (JSONObject) o;
				String VehicleNo = (String) bus.get("VehicleNo");
				String TripId = (String) bus.get("TripId");
				String RouteNo = (String) bus.get("RouteNo");
				String Direction = (String) bus.get("Direction");
				String Destination = (String) bus.get("Destination");
				String Pattern = (String) bus.get("Pattern");
				Double Latitude = (Double) bus.get("Latitude");
				Double Longitude = (Double) bus.get("Longitude");
				String RecordedTime = (String) bus.get("RecordedTime");
				JSONArray RouteMap = (JSONArray) bus.get("RouteMap");
				Iterator<String> it = RouteMap.iterator();
				String Href = null;
				while(it.hasNext())
				{
					Href = it.next();
				}
				busupdate.set(VehicleNo, TripId, RouteNo, Direction, Destination, Pattern, Latitude, Longitude, RecordedTime, Href);
			}
            
        }
		catch(FileNotFoundException fnfe) {fnfe.printStackTrace();}
		catch(IOException ioe) {ioe.printStackTrace();}
		catch(Exception e) {e.printStackTrace();}

	}
	public IStack<StopVO> listStops(Integer tripID) {
		// TODO Auto-generated method stub
		Stack<StopVO> stackstops = new Stack();
		Double[] l2 = busupdate.getLatLon(tripID);
		Iterator<zoneid> it = stops.iterator();
		while(it.hasNext())
		{
			zoneid zone = it.next();
			Iterator<StopVO> itz = zone.getList().iterator();
			while(itz.hasNext())
			{
				StopVO vo = itz.next();
				if((getDistance(l2[0], l2[1], vo.getLat(), vo.getLon())) <= 70)
				{
					stackstops.push(vo);
				}
			}
		}
		return stackstops;

	}
	public void loadStops() {
		// TODO Auto-generated method stub
		try
		{
			BufferedReader bf = new BufferedReader(new FileReader("/data/stops.txt"));
			String line = bf.readLine();
			stops = new RingList<zoneid>();
			while((line = bf.readLine()) != null)
			{
				Iterator<zoneid> it = stops.iterator();
				String[] data = line.split(",");
				boolean a = false;
				while(it.hasNext() && !a)
				{
					zoneid z = it.next();
					if(data[6].equals(z.zone))
					{
						StopVO stp = new StopVO();
						stp.set(data[0], data[1], data[2], data[3], Double.parseDouble(data[4]), Double.parseDouble(data[5]), data[6], data[7], data[8], data[9]);
						z.addStop(stp);
						a = true;
					}
				}
				if(!a)
				{
					StopVO stp = new StopVO();
					zoneid zone = new zoneid();
					stp.set(data[0], data[1], data[2], data[3], Double.parseDouble(data[4]), Double.parseDouble(data[5]), data[6], data[7], data[8], data[9]);
					zone.addStop(stp);
					stops.add(zone);
				}
			}
		}
		catch(IOException ioe) {ioe.printStackTrace();}
		catch(Exception e) {e.printStackTrace();}
	}
	
	public double getDistance(double lat1, double lon1, double lat2, double lon2)
	{
		// TODO Auto-generated method stub
        final int R = 6371*1000; // Radious of the earth
    
        Double latDistance = toRad(lat2-lat1);
        Double lonDistance = toRad(lon2-lon1);
        Double a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + 
                   Math.cos(toRad(lat1)) * Math.cos(toRad(lat2)) * 
                   Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2);
        Double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        Double distance = R * c;
         
        return distance;
     }
	
	private Double toRad(Double value) {
        return value * Math.PI / 180;
    }


}

