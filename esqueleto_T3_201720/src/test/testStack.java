package test;



import model.data_structures.Stack;
import junit.framework.TestCase;

public class testStack<E> extends TestCase 
{
  private Stack pila;
  
  public void escenario()
  {
	  pila = new Stack();
  }
  public void testPush()
  {
	  escenario();
	  pila.push("A");
	  assertEquals("No agrego el primer elemento correctamente","A", pila.pop());
	  pila.push("A");
	  pila.push("B");
	  pila.pop();
	assertEquals("No agrego el elemento correctamente","A",pila.pop());
  }
public void testPop()
{
	escenario();
	assertEquals("Deberia retornar null",null,pila.pop());
}
public void testSize()
{
	escenario();
	assertEquals("No conto el tamano correctamente",0,pila.size());
	pila.push("A");
	pila.push("B");
	pila.push("C");
	assertEquals("No conto el tamano correctamente",3,pila.size());
	
}

}
  