package test;

import model.data_structures.Queue;
import junit.framework.TestCase;

public class testQueue<E> extends TestCase 
{

	private Queue cola;
	
	public void escenario()
	{
		cola = new Queue();
	}
	public void testEnQueue()
	{
		escenario();
		 cola.enqueue("A");
		assertEquals("No agrego el elemento correctamente","A",cola.darItem(cola.first));
		assertEquals("No agrego el elemento correctamente","A",cola.darItem(cola.last));
		cola.enqueue("B");
		assertEquals("No agrego el elemento correctamente","B",cola.darItem(cola.last));
		assertEquals("No agrego el elemento correctamente","A",cola.darItem(cola.first));
	}
  public void testDeQueue()
  {
	  escenario();
	  assertEquals("No creo la cola vacia correctamnete",null,cola.dequeue());
	  cola.enqueue("A");
	  cola.enqueue("B");
	  assertEquals("No entrego el elemento correcto","A",cola.dequeue());
	  
  }
  public void testSize()
  {
	  escenario();
	  assertEquals("No conto los elementos correctamente",0,cola.size());
	 cola.enqueue("A");
	 cola.enqueue("B");
	 cola.enqueue("C");
	 assertEquals("No conto los elementos correctamente",3,cola.size());
  }

}
